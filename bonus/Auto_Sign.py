from exchangelib import Credentials, Account, Message, Mailbox
import logging
import re
import requests
# This handler will pretty-print and syntax highlight the request and response XML documents
from exchangelib.util import PrettyXmlHandler
from bs4 import BeautifulSoup

MAIL_LOGIN = ""  # ex : guillaume.duarte@imie.fr
MAIL_PASSWORD = ""  # jeanMi
MAIL_ADDRESS = ""  # ex : guillaume.duarte@imie.fr


def main():
    # logging.basicConfig(level=logging.DEBUG, handlers=[PrettyXmlHandler()])

    credentials = Credentials(MAIL_LOGIN, MAIL_PASSWORD)
    print("Setting up Campus academy account")
    account = Account(MAIL_ADDRESS, credentials=credentials, autodiscover=True)
    # You can also send emails. If you don't want a local copy:

    print("Printing Campus academy emails ...")
    for item in account.inbox.filter(subject__startswith='Demande de signature').order_by('-datetime_received')[:1]:
        if (item.subject):
            print('Found an email at : ', end='')
            print(item.datetime_received)
            # print(item.subject, item.sender, item.datetime_received)
            body_parsed = BeautifulSoup(item.body, features="lxml")
            mail_link = body_parsed.find('div', attrs={"class": "textGrey alignCenter"})
            mail_link = str(mail_link.get_text())
            start = int(mail_link.find('https://recovery.jesuisencours.com'))  # on essaie de recupere l'url de la div
            if start != -1:
                print('Found link to start auto sign :')
                mail_link = mail_link[start:]
                print(mail_link)
                r = requests.get(mail_link)
                if r.status_code:
                    print("Signature envoyé")


if __name__ == "__main__":
    # execute only if run as a script
    main()
