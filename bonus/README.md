## Auto émargement

### Ce script a été créer à titre purement éducatif et n'est pas utilisé dans un but malveillant 
#### (Je suis présent et à l'heure tout le temps de toute façon)

Pendant cette magnifique periode de confinement, Campus Academy a mit en place un systeme d'émargement par internet.

On reçoit le matin et en début d'après-midi, un mail à signer en accedant à l'url contenu dans le mail.

J'ai trouvé plusieurs failles permettant de facilement de faire un script qui s'en charge automatiquement.

### points d'amélioration //TODO

- Parfois le navigateur se prends un captcha, il faudrait utiliser un [anti-captcha](https://anticaptcha.atlassian.net/wiki/spaces/API/pages/623869953/RecaptchaV3TaskProxyless+Google+Recaptcha+v.3)
- Si le script doit signer pour plusieurs personnes -> utilisation de proxy de type non ISP


![Auto Sign](http://owncloud.24server.fr/s/hxd4Rn3RRKmmONn/download)