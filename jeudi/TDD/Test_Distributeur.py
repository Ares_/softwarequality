import unittest
import distributeur

class DistribTest(unittest.TestCase):

    def test_floatMoney( self ):
        money = type(distributeur.money)
        testmoney = type(45.3)
        self.assertEqual(money, testmoney)

    def test_stockNotNull( self ):
        quantity = distributeur.Product.quantity
        self.assertNotEqual(quantity, 0)

    def test_selectCharExist( self ):
        selection = distributeur.Product.id
        self.assertEqual(type(selection), type('a'))

    def test_giveBack( self ):
        spend = distributeur.spend
        giveBack = distributeur.give_back
        self.assertLess(giveBack, spend)

    def test_giveBackNotMinus( self ):
        giveBack = distributeur.give_back
        self.assertLess(0, giveBack)

    def test_transactionTrue( self ):
        spend = distributeur.spend
        cost = distributeur.Product.cost
        total = spend > cost
        self.assertEqual(total, True)
    
    def test_transactionFalse( self ):
        spend = distributeur.spend
        cost = distributeur.Product.cost
        total = spend > cost
        self.assertEqual( total, False )

if __name__ == "__main__":
    unittest.main()
