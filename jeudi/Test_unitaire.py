import unittest
from decodeur import splitter

class TestDecodeur(unittest.TestCase):

    # Initialisation des variables pour pouvoir les réutiliser dans les divers def()
    def setUp(self):
        stringTest = 'd,hgsjfdgh gksfjg'
        self.resultAttendu = ['d,hgsjfdgh', 'gksfjg']
        self.r = splitter(stringTest) 

    def test_basicTest(self):
        self.assertEqual(self.resultAttendu, self.r)

    def test_inTest(self):
        self.assertIn('gksfjg', self.r)
    
    @unittest.skip('Juste pour tester')
    def test_decoration(self):
        # def uniquement pour tester la décoration
        pass    
        
if __name__ == "__main__":
    unittest.main()


# -----------------------------------------------------------------------------
#    Résultat des tests :
#        test_basicTest (__main__.TestDecodeur) ... ok
#       test_decoration (__main__.TestDecodeur) ... skipped 'Juste pour tester'
#       test_inTest (__main__.TestDecodeur) ... ok
#        ----------------------------------------------------------------------
#        Ran 3 tests in 0.002s
#        OK (skipped=1)
# -----------------------------------------------------------------------------
