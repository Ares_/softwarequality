#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void triFusion(int i, int j, int tab[], int tmp[]) {
    if (j <= i) { return; }

    int m = (i + j) / 2;

    triFusion(i, m, tab, tmp);     // recursive left
    triFusion(m + 1, j, tab, tmp); //recursive right
    int pg = i;
    int pd = m + 1;
    int c;          // AI

    for (c = i; c <= j; c++) {
        if (pg == m + 1) { // limit size
            tmp[c] = tab[pd];
            pd++;
        } else if (pd == j + 1) { // limite size
            tmp[c] = tab[pg];
            pg++;
        } else if (tab[pg] < tab[pd]) { // tab ==
            tmp[c] = tab[pg];
            pg++;
        } else {  // tmp ==
            tmp[c] = tab[pd];
            pd++;
        }
    }
    for (c = i; c <= j; c++) { // tmp[] => <= tab[]
        tab[c] = tmp[c];
    }
}


int main() {
    char *buffer = 0;
    long length;
    FILE *f = fopen("../list.txt", "r");
    int numberCount = 0;
    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        fseek(f, 0, SEEK_SET);
        buffer = malloc(length);
        if (buffer) {
            fread(buffer, 1, length, f);
        }
        fclose(f);
    }

    if (buffer) {
        for (int i = 0; buffer[i] != '\0'; ++i) {
            if (buffer[i] == ' ')
                numberCount++;
        }
        //printf("%d", numberCount);
        char *ptr = strtok(buffer, " ");
        int i = 1;
        int tab[numberCount], tmp[numberCount];
        tab[0] = atoi(ptr);
        while (ptr != NULL) {
            ptr = strtok(NULL, " ");
            if(ptr != NULL){
                tab[i] = atoi(ptr);
            }
            //printf("%d\n", tab[i]);
            i++;
        }
        free(ptr);
        printf("nombre d'elements : %d", numberCount);
        clock_t begin = clock();
        triFusion(0, numberCount, tab, tmp);
        clock_t end = clock();
        double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
        printf("\n Tableau trié : ");
        /* for (i = 0; i < numberCount; i++) {
             printf(" %4d", tab[i]);
         }
         */
        printf("\n temps en sec : %f", time_spent);
    } else {
        printf("wtf is doing the buffer");
    }
    return 0;
}
