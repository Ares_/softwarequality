def main():
    inputstring = input("Saisissez une phrase : ")
    print(type(inputstring))  # on recupere la phrase

    splitedstring = inputstring.split()  # on separe la phrase en liste
    print(splitedstring)


if __name__ == '__main__':
    main()
