import os


def main():
    print("Saisissez un nombre")
    try:
        nb1 = float(input()) # on recupere le 1er nombre
    except ValueError:
        print("La valeur saisi n'est pas valable")
        os._exit(0)
    print("Saisissez un opérateur")
    operator = input()
    print("Saisissez un nombre")
    try:
        nb2 = float(input()) # on recupere le 2eme nombre
    except ValueError:
        print("La valeur saisi n'est pas valable")
        os._exit(0)

    # on effectue le calcul en fonction de l'operateur
    if operator == "-":
        res = nb1 - nb2
    elif operator == "+":
        res = nb1 + nb2
    elif operator == "*":
        res = nb1 * nb2
    elif operator == "/":
        if nb2 == 0:
            print("Impossible de diviser par 0")
        else:
            res = nb1 / nb2
    else:
        print("L'opérateur saisi n'est pas valable")
        os._exit(0)
    try:
        res
    except NameError:
        print("Erreur")
    else:
        print(nb1, operator, nb2, " = ", res)
        print("Saisir le nom du fichier pour la sauvegarde")
        filename = input()
        f = open(filename, "w")
        f.write("%f %s %f = %f" % (nb1, operator, nb2, res))
        f.close()


if __name__ == '__main__':
    main()
