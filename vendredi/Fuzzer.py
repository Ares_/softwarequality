import os
import subprocess
import sys
import time
from threading import Thread
from os import path

from fuzzingbook.Grammars import simple_grammar_fuzzer

# ------------------- Paramètres à changer -------------------
THREAD_NUMBER = 200
TEST_TODO = 1000
CALCULATOR_PATH = os.path.dirname(sys.argv[0]) + "/" + "calc.exe"  # Path to the calculator
# ------------------------------------------------------------

TEST_LIST = []
EXPR_GRAMMAR = {
    "<start>":
        ["<expr>"],

    "<expr>":
        ["<term> + <expr>", "<term> - <expr>", "<term>"],

    "<term>":
        ["<factor> * <term>", "<factor> / <term>", "<factor>"],

    "<factor>":
        ["+<factor>",
         "-<factor>",
         "(<expr>)",
         "<integer>.<integer>",
         "<integer>"],

    "<integer>":
        ["<digit><integer>", "<digit>"],

    "<digit>":
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
}


# Class pour permettre d'instancier le subprocess pour éviter qu'il soit call avant davoir fini le traitement précédent
class Threading(Thread):
    def __init__(self, thread_id, test_by_thread):
        self.test_list = TEST_LIST[thread_id * test_by_thread:thread_id * test_by_thread + test_by_thread]
        self.thread_id = thread_id
        self.test_by_thread = test_by_thread
        self.stderr = []
        self.param = None
        self.stdout = []
        Thread.__init__(self)

    def run(self):
        for i in self.test_list:
            self.param = i
            try:
                process = subprocess.check_output(f"{CALCULATOR_PATH} {self.param}", shell=False, stdin=subprocess.PIPE,
                                                  stderr=subprocess.STDOUT)
                self.stdout.append(process.decode('ascii'))
            except subprocess.CalledProcessError as e:
                self.stderr.append(self.param)


def main():
    if not path.exists(CALCULATOR_PATH):
        print("Calculatrice non trouvé merci de changer le CALCULATOR_PATH en haut du fichier")
        print("Current path : %s" % CALCULATOR_PATH)
        return
    time_start = time.time()
    print('Starting the Fuzzer ...')
    # Liste pour stocker tous les threads
    thread_list = []
    # on crée la liste de test a faire
    test_by_thread = round(TEST_TODO / THREAD_NUMBER)
    if THREAD_NUMBER > TEST_TODO:
        print("Erreur nombre de thread > nombre de test")
        return
    print("%i Tests à créer ..." % TEST_TODO)
    start = int(round(time.time() * 1000))
    for i in range(TEST_TODO):
        TEST_LIST.append(simple_grammar_fuzzer(grammar=EXPR_GRAMMAR, max_nonterminals=3))
        if int(round(time.time() * 1000)) - start > 3000:
            print("%i / %i" % (i, TEST_TODO))
            start = int(round(time.time() * 1000))

    print("Liste de test créer")
    # On créer les threads et on envoit la liste
    for i in range(THREAD_NUMBER):
        t = Threading(i, test_by_thread)
        thread_list.append(t)
    # le start pour lancer les threads
    for t in thread_list:
        t.start()

    print("%i Threads Créés et lancés" % THREAD_NUMBER)

    # le join pour faire attendre que tous les threads finissent
    for t in thread_list:
        t.join()

    print("On affiche le résultat")
    affichage = False
    for t in thread_list:
        # Affichage des grammaires qui ont plantées le programme
        for a in t.stdout:
            if a is not None and not affichage:
                print("Structure du programme : %s" % a)
                affichage = True
        for a in t.stderr:
            if a is not None:
                print("Retour d'erreur sur test : %s" % a)
    print("---------------------------------------------------")
    print("Temps écoulé pour %i tests : %s secondes" % (TEST_TODO, time.time() - time_start))
    print("Made with love and stubbornness by Mathieu MESNIL and Guillaume DUARTE")


if __name__ == "__main__":
    main()
